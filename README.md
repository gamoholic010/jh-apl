JH-APL
=========

OpenStack. Cloud computing security.

Here we are collaboratively developing a project, as a part of our Software Engineering Practicum Capstone, that will perform periodical validations of run-time integrity of OpenStack services and configurations such that trusted nodes are running known good code.

Directories structure:

* Client meetings artifacts

* Openstack research documents

* Process selection


![Logos][Logos]

[Logos]:https://bitbucket.org/gamoholic010/jh-apl/downloads/logos.png
[CMU]:http://cmu.edu/
[MSIT-SE]:http://mse.isri.cmu.edu/software-engineering/web3-programs/MSIT-SE/index.html
[JHUAPL]:http://www.jhuapl.edu/
